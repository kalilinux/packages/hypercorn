Source: hypercorn
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12), dh-python, python3-setuptools, python3-all, python3-sphinx, python3-toml, python3-h2, python3-h11, python3-pytest, python3-trio, python3-wsproto (>= 0.14.0), python3-asynctest, python3-typing-extensions, python3-priority, python3-pytest-asyncio
Standards-Version: 4.5.0
Homepage: https://gitlab.com/pgjones/hypercorn/
Vcs-Browser: https://gitlab.com/kalilinux/packages/hypercorn
Vcs-Git: https://gitlab.com/kalilinux/packages/hypercorn.git
Testsuite: autopkgtest-pkg-python

Package: python3-hypercorn
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-h2, python3-h11, python3-wsproto (>= 0.14.0)
Suggests: python-hypercorn-doc
Description: ASGI Server based on Hyper libraries (Python 3)
 This package contains an ASGI Server based on Hyper libraries and inspired by
 Gunicorn.
 .
 Hypercorn supports HTTP/1, HTTP/2, WebSockets (over HTTP/1 and HTTP/2),
 ASGI/2, and ASGI/3 specifications. Hypercorn can utilise asyncio, uvloop, or
 trio worker types.
 .
 This package installs the library for Python 3.

Package: python-hypercorn-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: ASGI Server based on Hyper libraries (common documentation)
 This package contains an ASGI Server based on Hyper libraries and inspired by
 Gunicorn.
 .
 Hypercorn supports HTTP/1, HTTP/2, WebSockets (over HTTP/1 and HTTP/2),
 ASGI/2, and ASGI/3 specifications. Hypercorn can utilise asyncio, uvloop, or
 trio worker types.
 .
 This is the common documentation package.
